import java.io.File;
import java.util.UUID;

import com.vividsolutions.jts.geom.Geometry;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.datasyslab.geospark.formatMapper.GeoJsonReader;
import org.datasyslab.geospark.serde.GeoSparkKryoRegistrator;
import org.datasyslab.geospark.spatialRDD.SpatialRDD;
import org.datasyslab.geosparksql.utils.Adapter;
import org.datasyslab.geosparksql.utils.GeoSparkSQLRegistrator;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;


public class GeoSparkExample {
    public static void main(String[] args) throws Exception {
        String pointsFile = new File(args[0]).getAbsolutePath();
        String subscriptionsFile = new File(args[1]).getAbsolutePath();
        String outputFile = new File(pointsFile).getParent() + File.separator + UUID.randomUUID().toString();

        Logger.getLogger("org").setLevel(Level.WARN);
        Logger.getLogger("akka").setLevel(Level.WARN);

        SparkConf conf = new SparkConf().setAppName("adapterTestJava").setMaster("local[*]");
        conf.set("spark.serializer", org.apache.spark.serializer.KryoSerializer.class.getName());
        conf.set("spark.kryo.registrator", GeoSparkKryoRegistrator.class.getName());
        conf.set("geospark.join.gridtype", "quadtree");
        conf.set("geospark.join.spatitionside", "right");


        JavaSparkContext sc = new JavaSparkContext(conf);

        SparkSession sparkSession = new SparkSession(sc.sc());

        GeoSparkSQLRegistrator.registerAll(sparkSession);

        SpatialRDD<Geometry> points = GeoJsonReader.readToGeometryRDD(sc, pointsFile, true, true);

        Dataset<Row> pointsDf = Adapter.toDf(points, sparkSession)
                .selectExpr("ST_GeomFromWKT(geometry) AS dataGeometry", "ID as dataId");

        SpatialRDD<Geometry> subscriptions = GeoJsonReader.readToGeometryRDD(sc, subscriptionsFile, true, true);

        Dataset<Row> subscriptionsDf = Adapter.toDf(subscriptions, sparkSession).selectExpr("ST_GeomFromWKT(geometry) AS subscriptionGeometry", "ID as subscriptionId");

        pointsDf.join(subscriptionsDf)
                .where("ST_Intersects(dataGeometry, subscriptionGeometry)")
                .groupBy("subscriptionId")
                .count()
                .repartition(1).write().json(outputFile);
    }
}